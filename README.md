# strongswan-sql
#### Configuration of StrongSwan SQL backend

This library allows for the manipulation of strongSwan connection configuration stored in a
MySQL database in a manner that is compatible with the strongSwan SQL plugin for charon.

* How to use this module:

The strongSwan IPsec package offers the means to store connection configuration in a
SQL database. This module offers some facilities to manipulate these config elements
from Haskell code in a simplified abstracted way.
This library offers two approaches to manipulating strongswan configuration in an
SQL database as expected by the SQL plugin. See _Managed_ vs _Manual_ API below.

* Managed API
Since managing each configuration object per hand and establishing the relationships
amongst them can be tricky and demands internal knowledge of the SQL plugin inner workings,
a special API is offered in which all configuration parameters are bundled together
in a single type (see `IPSecSettings`). The simplified API allows then for writing, reading
and deleting these, while behind bars the required elements are created and linked
together unbeknownst to the caller. This of course greatly simplifies things /but/ the
catch is that the ability to share configuration elements amongst connections is of
course lost. Each managed connection configuration gets a separate IKE, Child SA, Peer
config etc and no attempt is made to reuse them amongst managed connections.

* Manual API

The different strongswan configuration elements are mapped to a Haskell type and they
can be manually written or read from the SQL database. This offers utmost control in
terms of what elements get created and how they are interlinked. So for example one can
create a single IKE session configuration to be shared for all connections or have some
child SA configurations being shared amongst peers of a given type, etc. The downside
of course to this level of control is that it requires for the user of the library to be familiar with the (poorly documented) way in which the plugin expects the relationships to be expressed in terms of entries in the SQL tables etc.

The manual API has been reverse engineered based on the SQL table definitions available
[here](https://wiki.strongswan.org/projects/strongswan/repository/entry/src/pool/mysql.sql)

* __Child SA__ : All configuration parameters related to an IPsec SA.
* __IKE Configuration__ : Configuration applicable to the IKE session (/phase 1/ in IKEv1
parlance).
* __Peer Configuration__ : All elements related to configuration of a peering connection.
A peer connection links to a specific IKE configuration (by means of ID), and it is
furthermore associated to the Child SA by means of a `Peer2ChildConfig` type.
* __Traffic Selectors__: These are independent values linked to a Child SA by means of a
`Child2TSConfig` type.
