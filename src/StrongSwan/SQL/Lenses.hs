{-# LANGUAGE TemplateHaskell #-}

module StrongSwan.SQL.Lenses where

import Control.Lens             (makeLenses)
import StrongSwan.SQL.Types

makeLenses ''IKEConfig
makeLenses ''ChildSAConfig
makeLenses ''PeerConfig
makeLenses ''TrafficSelector
makeLenses ''SharedSecret
makeLenses ''SharedSecretIdentity
makeLenses ''IPSecSettings
